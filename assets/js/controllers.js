(function() {

  "use strict";

  var App = angular.module("App.controllers", ['ngSanitize']);

  App.filter("unsafe", function($sce) {
    return $sce.trustAsHtml;
  });

  App.filter("trustUrl", function($sce) {
    return function(url) {
      return $sce.trustAsResourceUrl(url);
    };
  });

 
  App.controller("MainCtrl", function ($scope, $http) {

    $scope.week1 = [];
    $scope.videoDetails = [];
    $scope.winnerDetails = [];
    $scope.week_cn = "第一周";
    $scope.week_en = "Week 1";

    $scope.changeLang = function(lang){
      if(lang == "en"){
        $(".eng-lang").css('display', 'block');
        $(".cn-lang").css('display', 'none');
        $(".select-language a").removeClass('active');
        $(".select-language #sghome-eng").addClass('active');

        sessionStorage.setItem('lang', 'en');
      } else {
        $(".eng-lang").css('display', 'none');
        $(".cn-lang").css('display', 'block');
        
        $(".select-language a").removeClass('active');
        $(".select-language #sghome-cn").addClass('active');

        sessionStorage.setItem('lang', 'cn');
      }
    }

    if(sessionStorage.getItem('lang')){
      if(sessionStorage.getItem('lang') === 'en'){
        $(".eng-lang").css('display', 'block');
        $(".cn-lang").css('display', 'none');

        $(".select-language a").removeClass('active');
        $(".select-language #sghome-eng").addClass('active');
        
      } else {
        $(".cn-lang").css('display', 'block');
        $(".eng-lang").css('display', 'none');

        $(".select-language a").removeClass('active');
        $(".select-language #sghome-cn").addClass('active');

      }
    } else {
      $(".cn-lang").css('display', 'block');
      $(".eng-lang").css('display', 'none');

      $(".select-language a").removeClass('active');
      $(".select-language #sghome-cn").addClass('active');

    }

  
    $scope.getWinnersForWeek1 = function(){
      $scope.week_cn = "第一周";
      $scope.week_en = "Week 1";
      $http({
        method: "GET",
        url: `assets/json/week1.json?${Date.now()}`,
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        },
        data: {}
      })
        .success(function($data) {

          if ($data) {
           $scope.week1 = $data.winners;
          } else {
          }
        })
        .error(function($data, $status, $headers, $config) {
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
    }

    // $scope.getWinnersForWeek1();

    $scope.getWinnersForWeek2 = function(){
      $scope.week_cn = "第二周";
      $scope.week_en = "Week 2";
      $http({
        method: "GET",
        url: `assets/json/week2.json?${Date.now()}`,
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        },
        data: {}
      })
        .success(function($data) {

          if ($data) {
           $scope.week2 = $data.winners;
          } else {
          }
        })
        .error(function($data, $status, $headers, $config) {
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
    }

    $scope.getWinnersForWeek2();

   
    $scope.getWinnersForWeek3 = function(){
      $scope.week_cn = "第三周";
      $scope.week_en = "Week 3";
      $http({
        method: "GET",
        url: `assets/json/week3.json?${Date.now()}`,
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        },
        data: {}
      })
        .success(function($data) {

          if ($data) {
           $scope.week3 = $data.winners;
          } else {
          }
        })
        .error(function($data, $status, $headers, $config) {
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
    }
    // $scope.getWinnersForWeek3();
   
    $scope.getWinnersForWeek4 = function(){
      $scope.week_cn = "第四周";
      $scope.week_en = "Week 4";
      $http({
        method: "GET",
        url: `assets/json/week4.json?${Date.now()}`,
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        },
        data: {}
      })
        .success(function($data) {

          if ($data) {
           $scope.week4 = $data.winners;
          } else {
          }
        })
        .error(function($data, $status, $headers, $config) {
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
    }
    $scope.getWinnersForWeek4();
    
   
    $scope.showWeek4Modal = function(username){
      username = username.replace(/\s/g,'');  
     
      $scope.week4.forEach(element => {
        
        if(element.username === username){
         $scope.videoDetails = element;
        }
     });
   
      $(`#${username}-modal`).modal('show');

      
    }
    
   
    $scope.getVideoDetails = function(username, week){
      $scope.videoDetails = [];

      if(week === "week1"){
        $scope.week1.forEach(element => {
        
          if(element.username === username){
           $scope.videoDetails = element;
          }
       });
      } else if (week === "week2"){
        $scope.week2.forEach(element => {
        
          if(element.username === username){
           $scope.videoDetails = element;
          }
       });
      } else if(week == 'week3'){
        $scope.week3.forEach(element => {
        
          if(element.username === username){
           $scope.videoDetails = element;
          }
       });
      } 
     

      $('#winnerModal').modal('show');

      $('#winnerModal').on('shown.bs.modal', () => {
        let links =  $(".caption").find('a');
        
        for(let i = 0; i < links.length; i++){
          let link = links[i];
          $(link).removeAttr('href')
        }
      });
      $('#winnerModal').on('hidden.bs.modal', () => {
        
        let element = $(".video-wrapper");
        $scope.stopVideo(element);
      });
    }

    $('#bebeModal').on('hidden.bs.modal', () => {
      let element = $(".video");
      $scope.stopVideo(element);
    });

    $scope.stopVideo = function ( element ) {
      let iframe = element.find( 'iframe')[0];
      
      let video = element.find( 'video' )[0];
    
      if ( iframe ) {
        iframe.src  = iframe.src;
      }
      if ( video ) {
        video.pause();
      }
    };
   
    $scope.showModal = function(username){
      // $('#week3modal').modal('show');
      $scope.week3.forEach(element => {
        
        if(element.username === username){
          $scope.vid_details = element;
        }
        
     });
     $('#week3modal').modal('show');
   
    
    }

    $('#week3modal').on('hidden.bs.modal', () => {
          
      let element = $(".video-wrapper");
      $scope.stopVideo(element);
    });

    $('#ol1v1a_kwok-modal').on('hidden.bs.modal', () => {
   
      let element = $("#ol1v1a_kwok-modal").find(".video-wrapper");
      $scope.stopVideo(element);
     
    });

    $('#monkeyjay13-modal').on('hidden.bs.modal', () => {
   
      let element = $("#monkeyjay13-modal").find(".video-wrapper");
      $scope.stopVideo(element);
     
    });
    $('#kt12-modal').on('hidden.bs.modal', () => {
   
      let element = $("#kt12-modal").find(".video-wrapper");
      $scope.stopVideo(element);
     
    });
    $('#ronlowphoto-modal').on('hidden.bs.modal', () => {
   
      let element = $("#ronlowphoto-modal").find(".video-wrapper");
      $scope.stopVideo(element);
     
    });
 
    $('#ElvisWY-modal').on('hidden.bs.modal', () => {
      // alert('hell')
      let element = $("#ElvisWY-modal").find(".video-wrapper");
      $scope.stopVideo(element);
     
     
    });

    $('#JohnWong-modal').on('hidden.bs.modal', () => {
      // alert('hell')
      let element = $("#JohnWong-modal").find(".video-wrapper");
      $scope.stopVideo(element);
     
     
    });
   
   
    $scope.getPopularWinners = function(){
      $http({
        method: "GET",
        url: `assets/json/popularwinners.json?${Date.now()}`,
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        },
        data: {}
      })
        .success(function($data) {

          if ($data) {
           $scope.popularWinners = $data.winners;
          } else {
          }
        })
        .error(function($data, $status, $headers, $config) {
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
    }

    $scope.getPopularWinners();

    $scope.getPopularWinnerVidDetails = function(username) {
      
      $scope.popularWinners.forEach(element => {
        
        if(element.username === username){
          $scope.winnerDetails = element;
          $('#popularWinnersmodal').modal('show');
        }
      });
     
    }

    $('#popularWinnersmodal').on('shown.bs.modal', () => {
      let links =  $(".caption").find('a');
      
      for(let i = 0; i < links.length; i++){
        let link = links[i];
        $(link).removeAttr('href')
      }
    });

    $('#popularWinnersmodal').on('hidden.bs.modal', () => {
        
      let element = $("#popularWinnersmodal .video-wrapper");
      $scope.stopVideo(element);
    });
    $scope.getGrandPrizeWinners = function(){
      $http({
        method: "GET",
        url: `assets/json/grandprizewinners.json?${Date.now()}`,
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        },
        data: {}
      })
        .success(function($data) {

          if ($data) {
           $scope.grandprize = $data;
          } else {
          }
        })
        .error(function($data, $status, $headers, $config) {
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
    }

    $scope.getGrandPrizeWinners();
   

  });


 

}());