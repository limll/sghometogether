<?php

$url='https://cmgpoll.herokuapp.com/answers';
print_r(get_data($url)); //dumps the content, you can manipulate as you wish to
 
/* gets the data from a URL */
 
function get_data($url)
{
	$ch = curl_init();
	$timeout = 10;
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}
?>